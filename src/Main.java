import java.util.*;

public class Main {

    Map<String,Student> map;
    public static void main(String[] args) {
        Main app = new Main();
        app.runTheApp();
    }

    private void runTheApp() {
        map = new HashMap<>();

        for(int i = 0;i<=10;i++){

            map.put(String.format("key %d",i),new Student(18+i,String.format("Student %d",i),String.format("St %d",i)));

        }

//        for(String key : map.keySet()){
//            System.out.println(key);
//            //System.out.println(map.get(key));
//        }
//        for(Student student:map.values()){
//            System.out.println(student);
//        }

       Set <Student> students = new TreeSet<>(new Comparator<Student>() {
           @Override
           public int compare(Student o1, Student o2) {
               return o2.getAge() - o1.getAge();
           }
       });
        students.addAll(map.values());
//        for(Student student: students){
//            System.out.println(student);
//        }
        Iterator<Student> iterator = students.iterator();
        while (iterator.hasNext()){
            Student st = iterator.next();
            if(st.getAge()%2 ==0){
                iterator.remove();
            }
        }
        for (Student student : students) {
            System.out.println(student);
        }
        System.out.println();
        for (Student student : map.values()) {
            System.out.println(student);
        }

    }
}
